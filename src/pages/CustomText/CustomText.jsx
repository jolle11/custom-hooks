import React from 'react';
import useInput from '../../hooks/useInput';

const CustomText = () => {
    const { textValue, newText } = useInput();
    return (
        <div className="custom-text">
            <h1>{textValue}</h1>
            <label>Introduce text below that will change the text above</label>
            <input type="text" onChange={newText} />
        </div>
    );
};

export default CustomText;

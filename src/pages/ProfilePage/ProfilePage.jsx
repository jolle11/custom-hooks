import React from 'react';
import useCounter from '../../hooks/useCounter';

const ProfilePage = () => {
    const { counter, add, subtract } = useCounter();
    return (
        <div className="profile-page">
            <div onClick={subtract}>Go down</div>
            <div onClick={add}>Add</div>
            <h1>{counter}</h1>
        </div>
    );
};

export default ProfilePage;

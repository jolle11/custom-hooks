import HomePage from './HomePage/HomePage';
import ProfilePage from './ProfilePage/ProfilePage';
import CustomText from './CustomText/CustomText';

export { HomePage, ProfilePage, CustomText };

import React from 'react';
import useCounter from '../../hooks/useCounter';

const HomePage = () => {
    const { counter, add, subtract } = useCounter();
    return (
        <div className="home-page">
            <h1>{counter}</h1>
            <button onClick={add}>Add</button>
            <button onClick={subtract}>Subtract</button>
        </div>
    );
};

export default HomePage;

import { useState } from 'react';

const useChrono = () => {
    const [time, setTime] = useState(0);

    const start = () => setInterval(setTime(time + 1), 1000);
    const reset = () => setTime(0);
    return { time, start, reset };
};

export default useChrono;

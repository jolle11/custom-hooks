import { useState } from 'react';

const useInput = () => {
    const [textValue, setTextValue] = useState('');
    const newText = (event) => setTextValue(event.target.value);
    return {
        textValue,
        newText,
    };
};

export default useInput;

import './App.scss';
import { ProfilePage, HomePage, CustomText } from './pages';
import { Routes, Route, Link } from 'react-router-dom';

function App() {
    return (
        <div className="app">
            <Link to="/">Home</Link>
            <Link to="/profile">Profile</Link>
            <Link to="/customtext">Custom</Link>
            <Routes>
                <Route path="/" element={<HomePage />}></Route>
                <Route path="/profile" element={<ProfilePage />}></Route>
                <Route path="/customtext" element={<CustomText />}></Route>
            </Routes>
        </div>
    );
}

export default App;
